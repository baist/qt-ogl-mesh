#ifndef MATH_HPP_
#define MATH_HPP_

#include <cmath>
#include <limits>

template<class T>
class Vector2D;
template<class T>
class Vector3D;

// advance comparing function
bool advcmp(int a, int b);
bool advcmp(float a, float b);
bool advcmp(double a, double b);

// calc a perpendicular on right side of the line
// a and b - start and points
template<class T>
Vector2D<T> calcNormalOfLine(const Vector2D<T> & a, const Vector2D<T> & b)
{
    Vector2D<T> res;
    res.x = b.y - a.y;
    res.y = -(b.x - a.x);
    return res.normalized();
}

// calc point on Quad Bezier curve
// start, middle, end points
// t - coefficient 0.0 ... 1.0
template<class T>
T calcPointByQuadBezier3(const T & startP, const T & midP, const T & endP,
                        float t)
{
    T a = startP + (midP - startP) * t;
    T b = midP + (endP - midP) * t;
    return a + (b - a) * t;
}

#endif // MATH_HPP_
