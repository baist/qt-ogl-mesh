#include "Triangle3D.hpp"

Triangle3D::Triangle3D()
{
}

Triangle3D::Triangle3D(Vector3Df _a, Vector3Df _b, Vector3Df _c):
    a(_a),
    b(_b),
    c(_c)
{
}

Triangle3D::~Triangle3D()
{
}

std::vector<Line3D> Triangle3D::lines() const
{
    std::vector<Line3D> res;
    res.push_back(Line3D{a, b});
    res.push_back(Line3D{b, c});
    res.push_back(Line3D{c, a});
    return res;
}

Vector3Df Triangle3D::normal() const
{
    Vector3Df edge1 = b - a;
    Vector3Df edge2 = c - a;
    return edge1.crossProduct(edge2).normalized();
}
