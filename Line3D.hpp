#ifndef LINE3D_HPP
#define LINE3D_HPP

#include "Vector3D.hpp"

// closed line segment
class Line3D
{
public:
    Vector3Df a;
    Vector3Df b;
public:
    Line3D()
    {
    }

    Line3D(const Vector3Df & _a, const Vector3Df & _b):
        a(_a),
        b(_b)
    {
    }

    ~Line3D()
    {
    }

    bool operator == (const Line3D & rhs) const
    {
        return (a == rhs.a) && (b == rhs.b);
    }
};

#endif // LINE3D_HPP
