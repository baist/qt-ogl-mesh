#include <algorithm>
#include <map>
#include "Util.hpp"

TriMesh makeTriCircle(float radius, unsigned int segments)
{
    TriMesh res;

    const Vector3Df centerPnt{0.0f, 0.0f, 0.0f};
    const float A_STEP = (float(FLT_PI) * 2.0f) / float(segments);

    for(unsigned int s = 0; s < segments; ++s) {
        const float angle = float(s) * A_STEP;
        const float nextAngle = float(s + 1) * A_STEP;
        Vector3Df pntA;
        pntA.x = std::sin(angle) * radius;
        pntA.y = 0.0f;
        pntA.z = std::cos(angle) * radius;
        pntA += centerPnt;
        Vector3Df pntB;
        pntB.x = std::sin(nextAngle) * radius;
        pntB.y = 0.0f;
        pntB.z = std::cos(nextAngle) * radius;
        pntB += centerPnt;
        res.insertTrianle(centerPnt, pntA, pntB);
    }

    return res;
}

TriMesh extrudeBevByY(const TriMesh & inMesh,
        float height, float outline, unsigned int segments)
{
    // clone current mesh with translating
    // there are top polygons of extruded mesh
    TriMesh resMesh = inMesh.translated(Vector3Df{0.0f, height, 0.0f});

    std::vector<EdgeIndx> boundEdges = inMesh.boundaryEdges();

    // calc average normals for each point.
    // normal describes direction
    std::map<size_t, Vector3Df> pntNrmls;
    for(const EdgeIndx & ei : boundEdges) {
        const Vector3Df & pntA = inMesh.point(ei.a);
        const Vector3Df & pntB = inMesh.point(ei.b);
        const Vector2Df tempNrml2D = calcNormalOfLine(
            Vector2Df {pntA.x, pntA.z}, Vector2Df {pntB.x, pntB.z} );
        const Vector3Df lineNrml =
            Vector3Df{tempNrml2D.x, 0.0f, tempNrml2D.y}; //.normalized();
        auto itA = pntNrmls.find(ei.a);
        if(pntNrmls.end() == itA) {
            pntNrmls.insert(std::pair<size_t, Vector3Df>(ei.a, lineNrml));
        }
        else {
            itA->second = (itA->second + lineNrml).normalized();
        }
        auto itB = pntNrmls.find(ei.b);
        if(pntNrmls.end() == itB) {
            pntNrmls.insert(std::pair<size_t, Vector3Df>(ei.b, lineNrml));
        }
        else {
            itB->second = (itB->second + lineNrml).normalized();
        }
    }

    for(auto const & pair : pntNrmls) {
        Vector3Df & pnt = resMesh.point(pair.first);
        const Vector3Df & nrml = pair.second;
        pnt = nrml * outline + pnt;
    }

    if(segments == 0) {
        return resMesh;
    }

    // create smooth connecting polygons
    const float DELTA = float(1) / segments;
    for(const EdgeIndx & ei : boundEdges) {
        const Vector3Df & inPntA = inMesh.point(ei.a);
        const Vector3Df & inPntB = inMesh.point(ei.b);
        const Vector3Df & resPntA = resMesh.point(ei.a);
        const Vector3Df & resPntB = resMesh.point(ei.b);
        const Vector3Df midPntA{inPntA.x, inPntA.y + height, inPntA.z};
        const Vector3Df midPntB{inPntB.x, inPntB.y + height, inPntB.z};
        for(unsigned int s = 0; s < segments; ++s) {
            const float delta0 = DELTA * float(s);
            const float delta1 = DELTA * float(s + 1);
            // quad points
            const Vector3Df & newPntA =
                    calcPointByQuadBezier3(
                        inPntA, midPntA, resPntA, delta0);
            const Vector3Df & newPntB =
                    calcPointByQuadBezier3(
                        inPntB, midPntB, resPntB, delta0);
            const Vector3Df & newPntC =
                    calcPointByQuadBezier3(
                        inPntB, midPntB, resPntB, delta1);
            const Vector3Df & newPntD =
                    calcPointByQuadBezier3(
                        inPntA, midPntA, resPntA, delta1);
            resMesh.addQuad(newPntA, newPntB, newPntC, newPntD);
        }
    }

    return resMesh;
}
