#if !defined(SINGLETON_HPP)
#define SINGLETON_HPP

// singleton pattern without autocreating object
template <typename T>
class Singleton
{
public:
	static T & inst()
	{
		return (*p_staticInstance);
	}
	static T * inst_ptr()
	{
		return p_staticInstance;
	}
public:
	Singleton()
	{
		p_staticInstance = static_cast<T*>(this);
	}
	~Singleton()
	{
		p_staticInstance = 0;
	}
protected:
	static T * p_staticInstance;
private:
	Singleton(const Singleton<T> &);
	Singleton& operator=(const Singleton<T> &);
};

#endif // SINGLETON_HPP
