#include "GLMesh.hpp"

GLMesh::GLMesh(std::shared_ptr<GLMeshReq> p_req):
    m_p_req(p_req)
{

}

void GLMesh::render()
{
    OGLFuncs & f = OGLFuncs::inst();

    QOpenGLVertexArrayObject::Binder vaoBinder(&m_p_req->vao);

    // always triangles
    f.glDrawElements(GL_TRIANGLES, m_p_req->vertexCount,
                   GL_UNSIGNED_SHORT, nullptr);
}
