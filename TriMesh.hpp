#ifndef TRIMESH_HPP
#define TRIMESH_HPP

#include <vector>
#include "Vector3D.hpp"
#include "Line3D.hpp"
#include "Triangle3D.hpp"

// indecies of edge
struct EdgeIndx
{
    size_t a;
    size_t b;
};

// indecies of triangle
struct TriIndx
{
    size_t a;
    size_t b;
    size_t c;
};

// triangulated 3D mesh with indecies
class TriMesh
{
public:
    TriMesh();
    ~TriMesh();
    Vector3Df & point(size_t index);
    const Vector3Df & point(size_t index) const;
    const std::vector<Vector3Df> & points() const;
    const std::vector<TriIndx> & triangles() const;
    Triangle3D triangle(size_t index) const;
    // integrate a triangle avoiding duplicate of points
    void insertTrianle(const Vector3Df & pntA,
                       const Vector3Df & pntB, const Vector3Df & pntC);
    // just add new triangle
    void addTrianle(const Vector3Df & pntA,
                const Vector3Df & pntB, const Vector3Df & pntC);
    // insert 2 triangles
    void insertQuad(const Vector3Df & pntA, const Vector3Df & pntB,
                    const Vector3Df & pntC, const Vector3Df & pntD);
    // add 2 triangles
    void addQuad(const Vector3Df & pntA, const Vector3Df & pntB,
                 const Vector3Df & pntC, const Vector3Df & pntD);
    // add other mesh
    void addMesh(const TriMesh & mesh);
    std::vector<EdgeIndx> edges() const;
    std::vector<EdgeIndx> boundaryEdges() const;
    // translate mesh and return it
    TriMesh translated (const Vector3Df & vec) const;
private:
    std::vector<Vector3Df> m_points;
    std::vector<TriIndx> m_triInds;
};

#endif // TRIMESH_HPP
