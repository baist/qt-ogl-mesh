#ifndef TRIANGLE3D_HPP
#define TRIANGLE3D_HPP

#include <vector>
#include "Vector3D.hpp"
#include "Line3D.hpp"

// 3D triangle (polygon)
class Triangle3D
{
public:
    Vector3Df a;
    Vector3Df b;
    Vector3Df c;
public:
    Triangle3D();
    Triangle3D(Vector3Df _a, Vector3Df _b, Vector3Df _c);
    ~Triangle3D();
    std::vector<Line3D> lines() const;
    Vector3Df normal() const;
};

#endif // TRIANGLE3D_HPP
