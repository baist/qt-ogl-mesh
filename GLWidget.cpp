#include <map>
#include "GLWidget.hpp"
#include <QMouseEvent>
#include <QOpenGLShaderProgram>
#include <QCoreApplication>

GLWidget::GLWidget(QWidget *parent):
    QOpenGLWidget(parent),
      m_xRot(0),
      m_yRot(0),
      m_zRot(0),
      m_p_program(nullptr)
{
}

GLWidget::~GLWidget()
{
    cleanup();
}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
    return QSize(400, 400);
}

static void qNormalizeAngle(int &angle)
{
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360 * 16)
        angle -= 360 * 16;
}

void GLWidget::setXRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_xRot) {
        m_xRot = angle;
        emit xRotationChanged(angle);
        update();
    }
}

void GLWidget::setYRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_yRot) {
        m_yRot = angle;
        emit yRotationChanged(angle);
        update();
    }
}

void GLWidget::setZRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_zRot) {
        m_zRot = angle;
        emit zRotationChanged(angle);
        update();
    }
}

void GLWidget::cleanup()
{
    makeCurrent();
    m_p_program.reset();
    m_p_glMesh.reset();
    doneCurrent();
}

static const char * vertexShaderSourceCore =
    "#version 150\n"
    "in vec4 position;\n"
    "uniform mat4 projMatrix;\n"
    "uniform mat4 mvMatrix;\n"
    "void main() {\n"
    "   gl_Position = projMatrix * mvMatrix * position;\n"
    "}\n";

static const char * fragmentShaderSourceCore =
    "#version 150\n"
    "out highp vec4 fragColor;\n"
    "void main() {\n"
    "   fragColor = vec4(1.0, 1.0, 1.0, 1.0);\n"
    "}\n";

void GLWidget::initializeGL()
{
    OGLFuncs & f = OGLFuncs::inst();
    // In this example the widget's corresponding top-level window can change
    // several times during the widget's lifetime. Whenever this happens, the
    // QOpenGLWidget's associated context is destroyed and a new one is created.
    // Therefore we have to be prepared to clean up the resources on the
    // aboutToBeDestroyed() signal, instead of the destructor. The emission of
    // the signal will be followed by an invocation of initializeGL() where we
    // can recreate all resources.
    connect(context(), &QOpenGLContext::aboutToBeDestroyed,
        this, &GLWidget::cleanup);

    f.initializeOpenGLFunctions();

    m_p_program =
        std::shared_ptr<QOpenGLShaderProgram>(new QOpenGLShaderProgram);
    m_p_program->addShaderFromSourceCode(
        QOpenGLShader::Vertex, vertexShaderSourceCore);
    m_p_program->addShaderFromSourceCode(
        QOpenGLShader::Fragment, fragmentShaderSourceCore);
    m_p_program->bindAttributeLocation("position", 0);
    m_p_program->link();

    m_p_program->bind();
    m_projMatrixLoc = m_p_program->uniformLocation("projMatrix");
    m_mvMatrixLoc = m_p_program->uniformLocation("mvMatrix");
    m_p_program->release();

    createMesh();

    // Our camera never changes in this case
    m_camera.setToIdentity();
    m_camera.translate(0, 0, -3);

    f.glClearColor(0, 0, 0, 1);
}

void GLWidget::paintGL()
{
    OGLFuncs & f = OGLFuncs::inst();

    f.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    f.glEnable(GL_DEPTH_TEST);
    f.glEnable(GL_CULL_FACE);

    m_world.setToIdentity();
    m_world.rotate(180.0f - (m_xRot / 16.0f), 1, 0, 0);
    m_world.rotate(m_yRot / 16.0f, 0, 1, 0);
    m_world.rotate(m_zRot / 16.0f, 0, 0, 1);

    m_p_program->bind();
    m_p_program->setUniformValue(m_projMatrixLoc, m_proj);
    m_p_program->setUniformValue(m_mvMatrixLoc, m_camera * m_world);

    f.glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    m_p_glMesh->render();

    m_p_program->release();
}

void GLWidget::resizeGL(int w, int h)
{
    m_proj.setToIdentity();
    m_proj.perspective(45.0f, GLfloat(w) / h, 0.01f, 1000.0f);
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    m_lastPos = event->pos();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - m_lastPos.x();
    int dy = event->y() - m_lastPos.y();

    if (event->buttons() & Qt::LeftButton) {
        setXRotation(m_xRot + 8 * dy);
        setYRotation(m_yRot + 8 * dx);
    } else if (event->buttons() & Qt::RightButton) {
        setXRotation(m_xRot + 8 * dy);
        setZRotation(m_zRot + 8 * dx);
    }
    m_lastPos = event->pos();
}

void GLWidget::createMesh()
{
    OGLFuncs & f = OGLFuncs::inst();

    TriMesh mesh = makeTriCircle(1.0f, 64);
    TriMesh extrMesh = extrudeBevByY(mesh, 0.125f, 0.125f, 8);

    std::shared_ptr<GLMeshReq> p_req =
            std::shared_ptr<GLMeshReq>(new GLMeshReq);

    p_req->vbo = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    p_req->ibo = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);

    // Create a vertex array object. In OpenGL ES 2.0 and OpenGL 2.x
    // implementations this is optional and support may not be present
    // at all. Nonetheless the below code works in all cases and makes
    // sure there is a VAO when one is needed.
    p_req->vao.create();
    p_req->vao.bind();

    // Setup our vertex buffer object.
    p_req->vbo.create();
    p_req->vbo.bind();
    auto const pnts = extrMesh.points();
    p_req->vbo.allocate( int(pnts.size() * sizeof(QVector3D)) );
    GLfloat * p_verts = reinterpret_cast<GLfloat *>(
        p_req->vbo.map(QOpenGLBuffer::WriteOnly));
    for(const Vector3Df & p : pnts) {
        *p_verts++ = p.x;
        *p_verts++ = p.y;
        *p_verts++ = p.z;
    }
    p_req->vbo.unmap();
    // Store the vertex attribute bindings for the program.
    f.glEnableVertexAttribArray(0);
    f.glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
        sizeof(QVector3D), nullptr);
    p_req->vbo.release();

    p_req->ibo.create();
    p_req->ibo.bind();
    auto const tris = extrMesh.triangles();
    p_req->ibo.allocate( int(tris.size() * 3 * sizeof(GLushort)) );
    GLushort * p_inds = reinterpret_cast<GLushort *>(
        p_req->ibo.map(QOpenGLBuffer::WriteOnly));
    for(const TriIndx & ti : tris) {
        *p_inds++ = GLushort(ti.a);
        *p_inds++ = GLushort(ti.b);
        *p_inds++ = GLushort(ti.c);
    }
    p_req->ibo.unmap();
    p_req->vertexCount = tris.size() * 3;

    p_req->vao.release();

    m_p_glMesh = std::shared_ptr<GLMesh>(new GLMesh(p_req));
}

