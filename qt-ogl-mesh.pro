QT += widgets

HEADERS       = \
    OGLFuncs.hpp \
    Singleton.hpp \
    GLWidget.hpp \
    MainWindow.hpp \
    GLMesh.hpp \
    Math.hpp \
    Vector3D.hpp \
    Vector2D.hpp \
    TriMesh.hpp \
    Triangle3D.hpp \
    Line3D.hpp \
    Util.hpp
SOURCES       = \
    main.cpp \
    OGLFuncs.cpp \
    GLWidget.cpp \
    MainWindow.cpp \
    GLMesh.cpp \
    Math.cpp \
    Vector2D.cpp \
    Vector3D.cpp \
    TriMesh.cpp \
    Triangle3D.cpp \
    Line3D.cpp \
    Util.cpp

#INSTALLS += target

FORMS += \
    MainWindow.ui
