#ifndef OGLFUNCS_HPP
#define OGLFUNCS_HPP

#include <QOpenGLFunctions>
#include <QOpenGLFunctions_3_2_Core>

#include "Singleton.hpp"

// all OpenGL 3.2 functions
class OGLFuncs: public Singleton<OGLFuncs>, public QOpenGLFunctions_3_2_Core
{
public:
    OGLFuncs();
    virtual ~OGLFuncs();
};

#endif // OGLFUNCS_HPP
