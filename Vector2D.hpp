#ifndef POINT2D_HPP
#define POINT2D_HPP

#include "Math.hpp"

template<class T>
class Vector2D
{
public:
    T x;
    T y;
public:
    Vector2D()
    {

    }
    explicit Vector2D(T _x, T _y):
        x(_x),
        y(_y)
    {

    }
    ~Vector2D()
    {

    }

    bool operator == (const Vector2D & rhs) const
    {
        return advcmp(x, rhs.x) && advcmp(y, rhs.y);
    }

    bool operator != (const Vector2D & rhs) const
    {
        return !(*this == rhs);
    }

    Vector2D<T> operator + (const Vector2D & vec) const
    {
        return Vector2D(x + vec.x, y + vec.y);
    }

    Vector2D<T> operator - (const Vector2D & vec) const
    {
        return Vector2D(x - vec.x, y - vec.y);
    }

    Vector2D<T> operator * (T scalar) const
    {
        return Vector2D(x * scalar, y * scalar);
    }

    Vector2D<T> operator * (const Vector2D & vec) const
    {
        return Vector2D(x * vec.x, y * vec.y);
    }

    Vector2D<T> operator / (T scalar) const
    {
        // TODO: handle error scalar=0
        T invScalar = 1 / scalar;
        return Vector2D(x * invScalar, y * invScalar);
    }

    Vector2D<T> operator / (const Vector2D & vec) const
    {
        return Vector2D(x / vec.x, y / vec.y);
    }

    const Vector2D<T> & operator + () const
    {
        return *this;
    }

    Vector2D<T> operator - () const
    {
        return Vector2D(-x, -y);
    }

    Vector2D<T> & operator += (const Vector2D & vec)
    {
        x += vec.x;
        y += vec.y;
        return *this;
    }
    Vector2D<T> & operator += (T scalar)
    {
        x += scalar;
        y += scalar;
        return *this;
    }

    Vector2D<T> & operator -= (const Vector2D & vec)
    {
        x -= vec.x;
        y -= vec.y;
        return *this;
    }

    Vector2D<T> & operator -= (T scalar)
    {
        x -= scalar;
        y -= scalar;
        return *this;
    }

    Vector2D<T> & operator *= (T scalar)
    {
        x *= scalar;
        y *= scalar;
        return *this;
    }

    Vector2D<T> & operator *= (const Vector2D & vec)
    {
        x *= vec.x;
        y *= vec.y;
        return *this;
    }

    Vector2D<T> & operator /= (T scalar)
    {
        // TODO: handle error scalar=0
        T invScalar = 1 / scalar;
        x *= invScalar;
        y *= invScalar;
        return *this;
    }

    Vector2D<T> & operator /= (const Vector2D & vec)
    {
        x /= vec.x;
        y /= vec.y;
        return *this;
    }

    T length () const
    {
        return std::sqrt(x * x + y * y);
    }

    Vector2D<T> normalized() const
    {
        T invLen = 1 / length();
        return Vector2D{x * invLen, y * invLen};
    }

    T dotProduct(const Vector2D & rhs) const
    {
        return x * rhs.x + y * rhs.y;
    }
};

typedef Vector2D<float> Vector2Df;

#endif // POINT2D_HPP
