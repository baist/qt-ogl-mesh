#ifndef MESH_HPP
#define MESH_HPP

#include <memory>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>

#include "OGLFuncs.hpp"

// requisites of mesh object
struct GLMeshReq
{
    QOpenGLVertexArrayObject vao;
    QOpenGLBuffer vbo;
    QOpenGLBuffer ibo;
    GLsizei vertexCount;
};

class GLMesh
{
public:
    GLMesh(std::shared_ptr<GLMeshReq> p_req);
    void render();
private:
    std::shared_ptr<GLMeshReq> m_p_req;
};

#endif // MESH_HPP
