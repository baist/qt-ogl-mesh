#ifndef POINT3D_HPP
#define POINT3D_HPP

#include "Math.hpp"

template<class T>
class Vector3D
{
public:
    T x;
    T y;
    T z;
public:
    Vector3D()
    {

    }
    explicit Vector3D(T _x, T _y, T _z):
        x(_x),
        y(_y),
        z(_z)
    {

    }
    ~Vector3D()
    {

    }

    bool operator == (const Vector3D & rhs) const
    {
        return advcmp(x, rhs.x) &&
               advcmp(y, rhs.y) &&
               advcmp(z, rhs.z);
    }

    bool operator != (const Vector3D & rhs) const
    {
        return !(*this == rhs);
    }

    Vector3D<T> operator + (const Vector3D & rhs) const
    {
        return Vector3D(x + rhs.x, y + rhs.y, z + rhs.z);
    }

    Vector3D<T> operator - (const Vector3D & rhs) const
    {
        return Vector3D(x - rhs.x, y - rhs.y, z - rhs.z);
    }

    Vector3D<T> operator * (T scalar) const
    {
        return Vector3D(x * scalar, y * scalar, z * scalar);
    }

    Vector3D<T> operator * (const Vector3D & rhs) const
    {
        return Vector3D(x * rhs.x, y * rhs.y, z * rhs.z);
    }

    Vector3D<T> operator / (T scalar) const
    {
        // TODO: handle error scalar=0
        T invScalar = 1.0 / scalar;
        return Vector3D(x * invScalar, y * invScalar, z * invScalar);
    }

    Vector3D<T> operator / (const Vector3D & rhs) const
    {
        return Vector3D(x / rhs.x, y / rhs.y, z / rhs.z);
    }

    const Vector3D<T> & operator + () const
    {
        return *this;
    }

    Vector3D<T> operator - () const
    {
        return Vector3D(-x, -y, -z);
    }

    Vector3D<T> & operator += (const Vector3D & rhs)
    {
        x += rhs.x;
        y += rhs.y;
        z += rhs.z;
        return *this;
    }
    Vector3D<T> & operator += (T scalar)
    {
        x += scalar;
        y += scalar;
        z += scalar;
        return *this;
    }

    Vector3D<T> & operator -= (const Vector3D & rhs)
    {
        x -= rhs.x;
        y -= rhs.y;
        z -= rhs.z;
        return *this;
    }

    Vector3D<T> & operator -= (T scalar)
    {
        x -= scalar;
        y -= scalar;
        z -= scalar;
        return *this;
    }

    Vector3D<T> & operator *= (T scalar)
    {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return *this;
    }

    Vector3D<T> & operator *= (const Vector3D & rhs)
    {
        x *= rhs.x;
        y *= rhs.y;
        z *= rhs.z;
        return *this;
    }

    Vector3D<T> & operator /= (T scalar)
    {
        // TODO: handle error scalar=0
        T invScalar = 1.0 / scalar;
        x *= invScalar;
        y *= invScalar;
        z *= invScalar;
        return *this;
    }

    Vector3D<T> & operator /= (const Vector3D & rhs)
    {
        x /= rhs.x;
        y /= rhs.y;
        z /= rhs.z;
        return *this;
    }

    T length () const
    {
        return std::sqrt(x * x + y * y + z * z);
    }

    Vector3D<T> normalized() const
    {
        T invLen = 1.0f / length();
        return Vector3D{x * invLen, y * invLen, z * invLen};
    }

    T dotProduct(const Vector3D & rhs) const
    {
        return x * rhs.x + y * rhs.y + z * rhs.z;
    }

    Vector3D<T> crossProduct(const Vector3D<T> & rhs) const
    {
        return Vector3D(y * rhs.z - z * rhs.y,
                        z * rhs.x - x * rhs.z,
                        x * rhs.y - y * rhs.x);
    }
};

typedef Vector3D<float> Vector3Df;

#endif // POINT3D_HPP
