#ifndef GLWIDGET_HPP
#define GLWIDGET_HPP

#include <memory>
#include <vector>
#include <QVector2D>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QMatrix4x4>

#include "Util.hpp"
#include "TriMesh.hpp"
#include "OGLFuncs.hpp"
#include "GLMesh.hpp"

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram)

class GLWidget: public QOpenGLWidget
{
    Q_OBJECT
public:
    GLWidget(QWidget *parent = nullptr);
    ~GLWidget();
    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;
public slots:
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);
    void cleanup();
signals:
    void xRotationChanged(int angle);
    void yRotationChanged(int angle);
    void zRotationChanged(int angle);
protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
private:
    OGLFuncs m_funcs;
    // angles for rotating
    int m_xRot;
    int m_yRot;
    int m_zRot;
    // last position of mouse pointer
    QPoint m_lastPos;
    std::shared_ptr<GLMesh> m_p_glMesh;
    QOpenGLVertexArrayObject m_vao;
    QOpenGLBuffer m_logoVbo;
    // shader program
    std::shared_ptr<QOpenGLShaderProgram> m_p_program;
    // id of projection matrix in GLSL
    int m_projMatrixLoc;
    // id of projection matrix in GLSL
    int m_mvMatrixLoc;
    // projection matrix
    QMatrix4x4 m_proj;
    // camera matrix
    QMatrix4x4 m_camera;
    // world matrix
    QMatrix4x4 m_world;
private:
    void createMesh();
};

#endif  // GLWIDGET_HPP
