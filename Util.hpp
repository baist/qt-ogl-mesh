#ifndef UTIL_HPP
#define UTIL_HPP

#include "Math.hpp"
#include "Vector2D.hpp"
#include "Vector3D.hpp"
#include "TriMesh.hpp"

// use our number PI for sure
const float FLT_PI = 3.141592653589793f;

// make flat triangulated circle in 3d
TriMesh makeTriCircle(float radius = 1.0f, unsigned int segments = 32);
// make extruded mesh from any mesh by Y axis
// height - extrusion value
// outline - offset factor
// segments - count of polygons
TriMesh extrudeBevByY(const TriMesh & inMesh,
        float height, float outline, unsigned int segments = 8);

#endif // UTIL_HPP
