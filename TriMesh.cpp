#include "TriMesh.hpp"

TriMesh::TriMesh()
{

}

TriMesh::~TriMesh()
{

}

Vector3Df & TriMesh::point(size_t index)
{
    return m_points.at(index);
}

const Vector3Df & TriMesh::point(size_t index) const
{
    return m_points.at(index);
}

const std::vector<Vector3Df> & TriMesh::points() const
{
    return m_points;
}

const std::vector<TriIndx> & TriMesh::triangles() const
{
    return m_triInds;
}

Triangle3D TriMesh::triangle(size_t index) const
{
    // TODO: implement taking a triangle
    return Triangle3D();
}

void TriMesh::insertTrianle(const Vector3Df & pntA,
                            const Vector3Df & pntB,
                            const Vector3Df & pntC)
{
    const size_t len = m_points.size();
    size_t indexA = len;
    size_t indexB = len;
    size_t indexC = len;
    for(size_t i = 0; i < len; ++i) {
        const Vector3Df & p = m_points.at(i);
        if(p == pntA) {
            indexA = i;
        }
        if(p == pntB) {
            indexB = i;
        }
        if(p == pntC) {
            indexC = i;
        }
    }
    if(indexA == len) {
        indexA = m_points.size();
        m_points.push_back(pntA);
    }
    if(indexB == len) {
        indexB = m_points.size();
        m_points.push_back(pntB);
    }
    if(indexC == len) {
        indexC = m_points.size();
        m_points.push_back(pntC);
    }
    m_triInds.push_back(TriIndx {indexA, indexB, indexC});
}

void TriMesh::addTrianle(const Vector3Df & pntA,
                         const Vector3Df & pntB,
                         const Vector3Df & pntC)
{
    const size_t len = m_points.size();
    m_points.push_back(pntA);
    m_points.push_back(pntB);
    m_points.push_back(pntC);
    m_triInds.push_back(TriIndx {len + 0, len + 1, len + 2});
}

void TriMesh::insertQuad(const Vector3Df & pntA, const Vector3Df & pntB,
                         const Vector3Df & pntC, const Vector3Df & pntD)
{
    // first triangle
    insertTrianle(pntA, pntB, pntC);
    // second triangle
    insertTrianle(pntC, pntD, pntA);
}

void TriMesh::addQuad(const Vector3Df & pntA, const Vector3Df & pntB,
                      const Vector3Df & pntC, const Vector3Df & pntD)
{
    // first triangle
    addTrianle(pntA, pntB, pntC);
    // second triangle
    addTrianle(pntC, pntD, pntA);
}

void TriMesh::addMesh(const TriMesh & mesh)
{
    const size_t len = m_points.size();
    m_points.insert(m_points.end(),
        mesh.m_points.begin(), mesh.m_points.end());
    for(const TriIndx & ti : mesh.m_triInds) {
        m_triInds.push_back(TriIndx {ti.a + len, ti.b + len, ti.c + len});
    }
}

std::vector<EdgeIndx> TriMesh::edges() const
{
    // just take edges from each triangle
    std::vector<EdgeIndx> egds;
    egds.reserve(m_triInds.size() * 3);
    for(const TriIndx & ti : m_triInds) {
        egds.push_back(EdgeIndx {ti.a, ti.b});
        egds.push_back(EdgeIndx {ti.b, ti.c});
        egds.push_back(EdgeIndx {ti.c, ti.a});
    }
    return egds;
}

std::vector<EdgeIndx> TriMesh::boundaryEdges() const
{
    // storage for not adjacent edges
    std::vector<EdgeIndx> resEdgs;
    resEdgs.reserve(m_triInds.size() * 3 / 2);

    for(size_t i = 0; i < m_triInds.size(); ++i) {
        const TriIndx & ti = m_triInds[i];
        const Triangle3D trgtTri(m_points[ti.a],
                m_points[ti.b], m_points[ti.c]);
        const Vector3Df trgtTriNormal =  trgtTri.normal();
        // adjacency for 3 edges
        bool foundAdgEdgeAB = false;
        bool foundAdgEdgeBC = false;
        bool foundAdgEdgeCA = false;
        for(size_t j = 0; j < m_triInds.size(); ++j) {
            if(i == j) {
                continue;
            }
            if (foundAdgEdgeAB && foundAdgEdgeBC && foundAdgEdgeCA) {
                break;
            }

            // calc collinearity of two triangles by comparing their normals
            const TriIndx & otherTI = m_triInds[j];
            const Triangle3D otherTri(m_points[otherTI.a],
                    m_points[otherTI.b], m_points[otherTI.c]);
            const Vector3Df otherTriNormal = otherTri.normal();
            if(trgtTriNormal != otherTriNormal) {
                continue;
            }

            // looking adjacent edges
            auto otherTriLns = otherTri.lines();
            // AB and BA lines
            for(const Line3D & ln : otherTriLns) {
                foundAdgEdgeAB |=
                        (ln == Line3D{trgtTri.a, trgtTri.b}) ||
                        (ln == Line3D{trgtTri.b, trgtTri.a});
            }
            // BC and CB lines
            for(const Line3D & ln : otherTriLns) {
                foundAdgEdgeBC |=
                        (ln == Line3D{trgtTri.b, trgtTri.c}) ||
                        (ln == Line3D{trgtTri.c, trgtTri.b});
            }
            // CA and AC lines
            for(const Line3D & ln : otherTriLns) {
                foundAdgEdgeCA |=
                        (ln == Line3D{trgtTri.c, trgtTri.a}) ||
                        (ln == Line3D{trgtTri.a, trgtTri.c});
            }
        }
        if (!foundAdgEdgeAB) {
            resEdgs.push_back(EdgeIndx {ti.a, ti.b});
        }
        if (!foundAdgEdgeBC) {
            resEdgs.push_back(EdgeIndx {ti.b, ti.c});
        }
        if (!foundAdgEdgeCA) {
            resEdgs.push_back(EdgeIndx {ti.c, ti.a});
        }
    }

    return resEdgs;
}

TriMesh TriMesh::translated(const Vector3Df & vec) const
{
    TriMesh res = *this;
    for(Vector3Df & p : res.m_points) {
        p += vec;
    }
    return res;
}
