#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

#include "ui_MainWindow.h"

class MainWindow: public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget * p_parent = nullptr);
private:
    Ui::MainWindow ui;
};

#endif  // MAINWINDOW_HPP
