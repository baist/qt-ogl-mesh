#include "Math.hpp"

// use our EPSILON for less sensitive calculation
const float FLT_EPSILON_ = 0.00005f;
const double DBL_EPSILON_ = 0.000000000000005;

bool advcmp(int a, int b)
{
    return a == b;
}

bool advcmp(float a, float b)
{
    return (std::fabs(a - b) < FLT_EPSILON_);
}

bool advcmp(double a, double b)
{
    return (std::fabs(a - b) < DBL_EPSILON_);
}
